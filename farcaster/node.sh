#!/bin/bash -e
clear
mainmenu() {
echo -e "\e[1;35m
┌─┐┌─┐┬─┐┌─┐┌─┐┌─┐┌┬┐┌─┐┬─┐
├┤ ├─┤├┬┘│  ├─┤└─┐ │ ├┤ ├┬┘
└  ┴ ┴┴└─└─┘┴ ┴└─┘ ┴ └─┘┴└─\e[0m" 
    echo
    echo " 💿 | 1.Install Node"
    echo " 🩺 | 2.View Graphana URL"
    echo " 📖 | 3.View Logs"
    echo " 🔄 | 4.Update Node"
    echo " 🗑️  | 5.Delete Node"
    echo "  ----------------"
    echo "  0.Back | 10.Exit"
    echo "  ----------------"
    echo -ne "Input: "
    read -r ans

    case $ans in
        1) install ;;
        2) graphana ;;
        3) logs ;;
        4) update ;;
        5) delete ;;
        0) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/testnet.sh) ;;
        10) echo "Bye bye." && exit ;;
        *) clear && echo "Invalid input!" && mainmenu ;;
    esac
}

install() { 
# Install farcaster
echo && echo -e "\e[1;32mDo you want to install Farcaster-node? (y/n)\e[0m"
read -r ans
if [ "$ans" != "y" ]; then
    mainmenu
fi

# update and install dependencies
sudo apt-get update && sudo apt-get install ca-certificates curl git jq lz4 build-essential screen -y

# install docker
# create docker keyring
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# add docker repo 
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# install docker
sudo apt-get update && sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y

# allow ports
ufw allow 2281
ufw allow 2283

# install hubble
curl -sSL https://download.thehubble.xyz/bootstrap.sh | bash
echo && echo
echo -e "\e[1;32mInstallation complete\e[0m"
echo && echo
mainmenu
}

delete() {
    echo && echo -e "\e[1;32mDo you want to delete Farcaster-node? (y/n)\e[0m"
    read -r ans
    if [ "$ans" != "y" ]; then
        mainmenu
    fi
    cd ~/hubble && ./hubble.sh down && cd
    echo && echo -e "\e[1;32mDeletion complete\e[0m"
    echo && echo
    mainmenu
}

update() {
  echo && echo -e "\e[1;32mDo you want to update Farcaster-node? (y/n)\e[0m"
  read -r ans
  if [ "$ans" != "y" ]; then
      mainmenu
  fi
cd ~/hubble && ./hubble.sh upgrade
mainmenu
}


graphana() {
ip=$(curl -s https://ipinfo.io/ip)
echo && echo -e "\nFarcaster node dashboard is accessible at:"
echo -e "http://$ip:3000"
mainmenu
}

logs() {
    docker logs hubble-hubble-1 --tail 350 -f
    mainmenu
}

mainmenu